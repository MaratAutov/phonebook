var gui = require('nw.gui');
var request = require('request');

var content = document.getElementById('content');
var menu = document.getElementById('menu');
var filter = document.getElementById('filter');
var phonesHeader = document.getElementById('phonesHeader');
var tableBody = document.getElementById('tableContent');

window.onload = window.onresize = () => {
	var padding = 10;
	var menuHeight = parseInt(menu.offsetHeight);
	var filterHeight = parseInt(filter.offsetHeight);

	content.style.padding = padding + 'px';
	content.style.width = menu.style.width + 'px';
	content.style.height = window.innerHeight - menuHeight - filterHeight - 50 + 'px';
	tableBody.style.height = window.innerHeight - 200 + 'px';
};

/*
 Очистить строки таблицы справочника
 */
clearTable = () => {
	var rowCount = tableBody.rows.length;
	for (var i = 0; i < rowCount; i++) {
    	tableBody.deleteRow(0);
	}
}

/*
 Добавить ячейку с текстовыми данными в строку.
 Параметры: 
 	tr - строка таблицы
 	data - текстовые данные
*/ 	
var appendOrdinalCell = (tr, data) => {
	var td = document.createElement('TD')
	td.appendChild(document.createTextNode(data));
	tr.appendChild(td);
	return td;
};

/*
 Добавить ячейку с кнопкой в строку.
 Параметры: 
 	tr - строка таблицы
 	data - название кнопки
 	cls - CSS-класс
 	func - Обрабочик нажатия на кнопку
*/ 	
var appendButtonCell = (tr, data, cls, func) => {
	var td = document.createElement('TD')
	var span = document.createElement('SPAN');
	span.setAttribute('class', cls);
	span.setAttribute('width', '100%');
	span.onclick = func;
	span.appendChild(document.createTextNode(data));
	td.appendChild(span);
	tr.appendChild(td);
	return td;
};

/*
 Добавить строку в таблицу
 Параметры:
 	id - Идентификатор
 	fio - Фамилия Имя Отчество
 	address - Адрес
 	phone - Телефон
*/ 	
var appendRow = (id, fio, address, phone) => {
    var tr = document.createElement('TR');
  	appendOrdinalCell(tr, id);
  	appendOrdinalCell(tr, fio);
  	appendOrdinalCell(tr, address);
  	appendOrdinalCell(tr, phone);
  	
  	// Кнопка редактирования записи
  	appendButtonCell(tr, "Редактировать", 'buttonGreen', () => {
  		// Загружаем данные записи в соответствующие поля формы
		var recordIdField = document.getElementById('recordId');
		recordIdField.value = id;
		var fioField = document.getElementById('filterFio');
		fioField.value = fio;
		var addressField = document.getElementById('filterAddress');
		addressField.value = address;
		var phoneField = document.getElementById('filterPhone');
		phoneField.value = phone;

		// Устанавливаем фокус ввода на первое поле
		// Все готово к редатированию
		fioField.focus();		
	});

	// Кнопка редактирования записи
  	appendButtonCell(tr, "Удалить", 'buttonRed', () => {
  		// Запрос на удаление
  		if (confirm('Удалить запись ?')) {
			deleteFromDB(id);
			// Если данные удаляемой записи отображаются в полях формы,
			// то эти поля надо очистить
			var recordIdField = document.getElementById('recordId');
			if (id == recordIdField.value) {
				document.getElementById('filterFio').value = "";
				document.getElementById('filterAddress').value = "";
				document.getElementById('filterPhone').value = "";
			}
		}
	});
  	return tr;
}

/*
 Загрузить данные, полученные с сервера в таблицу
 Параметры:
 	resultset - Список записей
*/
var loadDataToTable = (resultset) => {
	// Очистить таблицу
	clearTable();
	// Заполнить таблицу данными
	var len = resultset.length, i;
	try {
  		for (i = 0; i < len; i++) {
		    var item = resultset[i];
		    tableBody.appendChild(appendRow( item.id, item.fio, item.address, item.phone));
  		}
	} catch (e) {
		alert("loading has thrown an exception: "+e);
	}
};

/*
 Получить данные от удаленного сервера
 Параметры:
 	route - Маршрут, который добавляется к целевой url.
 	formParam - Параметры, передаваемые в методе POST. Могут отсутствовать.
 	callback - Функция обратного вызова. Вызывается в случае положительного ответа с сервера. 
 				Может отсутствовать.
*/
var fetchData = (route, formParam = null, callback = null) => {
	// Готовим запрос к серверу и вызываем его
	request.post({
		method: 'POST',
		url: gui.App.manifest.url + "/" + route,
		form: formParam
	}, (error, response, body) => {
		if (error) {
			alert(error);
  		} else if (response.statusCode == 200) {
  			try {
	  			if (callback == null) {	
	    			var info = JSON.parse(body);
	    			loadDataToTable(info);
	    		} else {
	    			callback();
	    		}
	    	} catch (e) {
	    		alert('The method fetchData has thrown an exception: ' + e);
	    	}
  		}
	});
};

/*
 Загрузить справочник
*/
var loadFromDB = () => {
	fetchData('list');
};

/*
 Поиск в справочнике
*/
var search = () => {
	// Соберем данные для поиска
	var fio = document.getElementById('filterFio');
	var address = document.getElementById('filterAddress');
	var phone = document.getElementById('filterPhone');

	// Проверим длину поисковых критериев
	if (fio.value.length < 3 && address.value.length < 3 && phone.value.length < 3) {
		alert('Поисковые критерии не заданы или длина заданных значений меньше 3!');
		return;
	} 

	// Формируем данные запроса
	var data = {fio: fio.value, address: address.value, phone: phone.value};
	// Ищем
	fetchData('find', data);
}

/*
 Добавить запись в справочник
*/
var appendDB = () => {
	// Соберем данные для добавления
	var fio = document.getElementById('filterFio');
	var address = document.getElementById('filterAddress');
	var phone = document.getElementById('filterPhone');

	// Формируем данные для добавления
	var data = {fio: fio.value, address: address.value, phone: phone.value};
	// Добавляем запись в справочник и обновляем таблицу
	fetchData('add', data, () => fetchData('list'));
}

/*
 Редактировать запись в справочнике
*/
var editDB = () => {
	// Соберем данные для редактирования
	var id = document.getElementById('recordId');
	var fio = document.getElementById('filterFio');
	var address = document.getElementById('filterAddress');
	var phone = document.getElementById('filterPhone');

	// Формируем данные для редактирования
	var data = {id: id.value, fio: fio.value, address: address.value, phone: phone.value};
	// Обновляем запись в справочнике и обновляем таблицу
	fetchData('update', data, () => fetchData('list'));
}

/*
 Удалить запись из справочника
 Параметры:
 	id - Идентификатор удаляемой записи
*/
var deleteFromDB = (id) => {
	// Удаляем запись на сервере и обновляем таблицу
	fetchData('delete/'+id, null, () => fetchData('list'));
}

// Теперь загрузим справочник
loadFromDB();
