// Модуль работы с БД

var JDBC = require('jdbc');
var jinst = require('jdbc/lib/jinst');

if (!jinst.isJvmCreated()) {
  jinst.addOption("-Xrs");
  jinst.setupClasspath(['./drivers/h2-1.4.197.jar']);
}

// В качестве БД используем H2 
// http://www.h2database.com
var db = new JDBC({
  // Параметры подключения к БД
  url: 'jdbc:h2://tmp/phone_book',
  minpoolsize: 5,
  maxpoolsize: 10,
  properties: {
    user : 'SA',
    password: ''
  }
});

function reserve(callback) {
  db.reserve((err, connobj) => {
    if (err) {
      return callback(err);
    } else {
      return callback(null, connobj, connobj.conn);
    }
  });
};

function release(connobj, err, result, callback) {
  db.release(connobj, (e) => {
    if (err) {
      return callback(err);
    } else {
      return callback(null, result);
    }
  });
};

exports.prepare = function(sql, callback) {
  reserve( (err, connobj, conn) => {
    conn.prepareStatement(sql, (err, preparedstatement) => {
      release(connobj, err, preparedstatement, callback);
    });
  });
};

exports.prepareCall = function(sql, callback) {
  reserve( (err, connobj, conn) => {
    conn.prepareCall(sql, (err, callablestatement) => {
      release(connobj, err, callablestatement, callback);
    });
  });
};

exports.update = function(sql, callback) {
  reserve( (err, connobj, conn) => {
    conn.createStatement( (err, statement) => {
      if (err) {
        release(connobj, err, null, callback);
      } else {
        statement.executeUpdate(sql, (err, result) => {
          release(connobj, err, result, callback);
        });
      }
    });
  });
};

exports.select = function(sql, callback) {
  reserve( (err, connobj, conn) => {
    conn.createStatement( (err, statement) => {
      if (err) {
        release(connobj, err, null, callback);
      } else {
        statement.executeQuery(sql, (err, result) => {
          release(connobj, err, result, callback);
        });
      }
    });
  });
};

exports.tableexists = function(catalog, schema, name, callback) {
  reserve( (err, connobj, conn) => {
    conn.getMetaData( (err, metadata) => {
      if (err) {
        release(connobj, err, null, callback);
      } else {
        metadata.getTables(catalog, schema, name, null, (err, resultset) => {
          if (err) {
            release(connobj, err, null, callback);
          } else {
            resultset.toObjArray( (err, results) => {
              release(connobj, err, results.length > 0, callback);
            });
          }
        });
      }
    });
  });
};

exports.metadata = function(callback) {
  reserve( (err, connobj, conn) => {
    conn.getMetaData( (err, metadata) => {
      release(connobj, err, metadata, callback);
    });
  });
};

// Инициализация
db.initialize( (err) => {
  if (err) {
    console.log(err);
  } else {
    // После подключения к БД, проверяем существование таблицы с 
    // данными справочника. Если таблицы нет, то ее надо создать.
  	this.update(
		'CREATE TABLE IF NOT EXISTS PHONE_BOOK ( \
		    ID INT AUTO_INCREMENT PRIMARY KEY, \
		    FIO VARCHAR(150), \
		    ADDRESS VARCHAR(200), \
		    PHONE VARCHAR(20) )',
		(err, result) => {
			if (err) {
		    	console.log(err);
		  	} else {
				console.log(`table has created. Result is ${result}`);
			}
		});
  	console.log('Database has initialized');
  }
});


