// server
const express        = require('express');
const app            = express();
const dbUtils        = require('./db');

var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: true }));

const port = 8080;
app.listen(port, () => {
  console.log('Started on ' + port);
});

var phoneRouter = require('./router');
app.use('/telephone-dir', phoneRouter);

