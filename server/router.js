var express = require('express');
var dbUtils = require('./db');
var router = express.Router();
var async = require('async');

// Заполнение списка
var fillResult = (results = []) => {
	let list = []
	for (let i = 0; i < results.length; i++) {
		list.push({id: results[i].ID, fio: results[i].FIO, address: results[i].ADDRESS, phone: results[i].PHONE });				
	}
	return list;
};

router.get('/', (req, res) => {
	res.send('Phone book');
});

// Найти записи, удовлетворяющие задачаемым критериям
// Если критерии не заданы, то возвращаются все записи
router.post('/list|find',  (req, res) => {
	// Заготовка запроса к БД
	var sql = "SELECT * FROM PHONE_BOOK WHERE 1 = 1";
	// Список параметров для запроса
	var callSeq = [];
	// Критерии отбора
	var {fio, address, phone} = req.body

	// Проверяем критерии и формируем запрос к БД
	if ((fio || "") !== "") {
		sql += ' AND UPPER(FIO) LIKE ?'
	} 
	if ((address || "") !== "") {
		sql += ' AND UPPER(ADDRESS) LIKE ?'
	} 
	if ((phone || "") !== "") {
		sql += ' AND UPPER(PHONE) LIKE ?'
	} 

	// Подготавливаем запрос к исполнению
	dbUtils.prepareCall(sql, 
		(err, preparedstatement) => {
			var n = 1;
			if ((fio || "") !== "") {
				callSeq.push((c) => preparedstatement.setString(n++, "%" + fio.toUpperCase() + "%", c));
			} 
			if ((address || "") !== "") {
				callSeq.push((c) => preparedstatement.setString(n++, "%" + address.toUpperCase() + "%", c));
			} 
			if ((phone || "") !== "") {
				callSeq.push((c) => preparedstatement.setString(n++, "%" + phone.toUpperCase() + "%", c));
			} 

			callSeq.push((c) => preparedstatement.executeQuery(c));
			// Выполняем запрос
			async.waterfall(callSeq,
				(err, resultset) => {
					if (err) {
						// Что-то пошло не так...
		        		res.send({ 'error': `An error has occurred ${err}` }); 
	    				console.log(err);
					} else {
						// Формируем ответ
						resultset.toObjArray( (err, results) => {
							res.json(fillResult(results));
						});
					}
				});
		});
});

// Добавить запись в справочник
router.post('/add', (req, res) => {
	// Добавляемые данные
	var {fio, address, phone} = req.body
	// Функция, возвращающая идентификатор добавленной записи
	var sendInsertedId = () => {
		dbUtils.select("SELECT SCOPE_IDENTITY() as ID;", 
			(err, resultset) => {
      			if (err) {
					// Что-то пошло не так...
      				res.send({ 'error': `An error has occurred ${err}` }); 
					console.log(err);
      			} else {
					resultset.toObjArray( (err, results) => {
          				if (results.length > 0) {
            				console.log(`ID: ${results[0].ID}`);
          				}
          				// Вернем идентификатор
    	  				res.json({ id: results[0].ID });
        			});
      			}
      		});
		};

	// Подготавливаем оператор вставки	
    dbUtils.prepareCall(
    	"INSERT INTO PHONE_BOOK (ID, FIO, ADDRESS, PHONE) VALUES (NULL, ?, ?, ?)", 
    	(err, preparedstatement) => {
    		// Выполняем подготовленный оператор вставки
    		async.waterfall([
    			(c) => preparedstatement.setString(1, fio, c),
				(c) => preparedstatement.setString(2, address, c),
				(c) => preparedstatement.setString(3, phone, c),
				(c) => preparedstatement.executeUpdate(c)],
				(err, result) => {
					if (err) {
        				res.send({ 'error': `An error has occurred ${err}` }); 							            	
						console.log(err);
					} else {
						console.log(`inserted ${result} record`);
						sendInsertedId();
					}
	    		});
    			
    	});
});

// Редатировать запись в справочнике
router.post('/update', (req, res) => {
	// Редактируемые данные
	var {id, fio, address, phone} = req.body

	// Подготавливаем оператор обновления данных	
    dbUtils.prepareCall(
    	"UPDATE PHONE_BOOK SET FIO = ?, ADDRESS = ?, PHONE = ? WHERE ID = ?", 
    	(err, preparedstatement) => {
    		// Выполняем подготовленный оператор обновления
    		async.waterfall([
				(c) => preparedstatement.setString(1, fio, c),
				(c) => preparedstatement.setString(2, address, c),
				(c) => preparedstatement.setString(3, phone, c),
    			(c) => preparedstatement.setInt(4, parseInt(id), c),
				(c) => preparedstatement.executeUpdate(c)],
				(err, result) => {
					if (err) {
						// Что-то пошло не так...
        				res.send({ 'error': `An error has occurred ${err}` }); 							            	
						console.log(err);
					} else {
						// Все хорошо
						console.log(`updated ${result} record`);
						res.json({res: 'ok'});
					}
	    		});
    			
    	});
});

// Удалить запись из справочника
router.post('/delete/:recordId', (req, res) => {
    dbUtils.prepareCall(
    	"DELETE FROM PHONE_BOOK WHERE ID = ?", 
    	(err, preparedstatement) => {
    		// Выполняем подготовленный оператор удаления
    		async.waterfall([ 
    			// Идентификатор удаляемой записи получаем из параметров 
    			(c) => preparedstatement.setInt(1, parseInt(req.params.recordId), c),
    			(c) => preparedstatement.executeUpdate(c)],
				(err, result) => {
		            if (err) {
						// Что-то пошло не так...
		            	res.send({ 'error': `An error has occurred ${err}` }); 
		            	console.log(err);
		            } else {
		            	// Все хорошо
						console.log(`delete record ID = ${req.params.recordId}`);
						res.json({res: 'ok'});
					}
				});
    	});
});

module.exports = router;
