var content = document.getElementById('content');
var menu = document.getElementById('menu');
var filter = document.getElementById('filter');
var phonesHeader = document.getElementById('phonesHeader');
var tableBody = document.getElementById('tableContent');

var db = openDatabase('mydb', '1.0', 'PhoneBook', 2 * 1024 * 1024);
db.transaction( function (tx) {
  	tx.executeSql('CREATE TABLE IF NOT EXISTS  PHONE_BOOK (id integer primary key, fio text, address text, phone text)');
});

window.onload = window.onresize = function () {
	var padding = 10;
	var menuHeight = parseInt(menu.offsetHeight);
	var filterHeight = parseInt(filter.offsetHeight);

	content.style.padding = padding + 'px';
	content.style.width = menu.style.width + 'px';
	content.style.height = window.innerHeight - menuHeight - filterHeight - 50 + 'px';
	tableBody.style.height = window.innerHeight - 200 + 'px';
};

function clearTable() {
	var rowCount = tableBody.rows.length;
	for (var i = 0; i < rowCount; i++) {
    	tableBody.deleteRow(0);
	}
}

var appendOrdinalCell = function(tr, data) {
	var td = document.createElement('TD')
	td.appendChild(document.createTextNode(data));
	tr.appendChild(td);
	return td;
};

var appendButtonCell = function(tr, data, cls, func) {
	var td = document.createElement('TD')
	var span = document.createElement('SPAN');
	span.setAttribute('class', cls);
	span.setAttribute('width', '100%');
	span.onclick = func;
	span.appendChild(document.createTextNode(data));
	td.appendChild(span);
	tr.appendChild(td);
	return td;
};

var appendRow = function(id, fio, address, phone) {
    var tr = document.createElement('TR');
  	appendOrdinalCell(tr, id);
  	appendOrdinalCell(tr, fio);
  	appendOrdinalCell(tr, address);
  	appendOrdinalCell(tr, phone);
  	appendButtonCell(tr, "Редактировать", 'buttonGreen', function () {
		console.log('on edit ' + id);
	});
  	appendButtonCell(tr, "Удалить", 'buttonRed', function () {
  		if (confirm('Удалить запись ?')) {
    		// Save it!
		} else {
    		// Do nothing!
		}
	});
  	return tr;
}

var fetchData = function (sql, parameters) {
	clearTable();
	db.transaction(function (tx) {
		tx.executeSql(sql, parameters, function (tx, results) {
	  		var len = results.rows.length, i;
	  		for (i = 0; i < len; i++) {
			    var item = results.rows.item(i);
			    tableBody.appendChild(appendRow( item.id, item.fio, item.address, item.phone));
	  		}
		});
	});

}
var loadFromDB = function () {
	fetchData('SELECT * FROM PHONE_BOOK', []);
};

async function insertRecord(fio, address, phone) {
	var id;
	await db.transaction(function (tx) {
		tx.executeSql('INSERT INTO PHONE_BOOK (id, fio, address, phone) VALUES (?, ?, ?, ?)', 
			[null, fio, address, phone]);
	  	
	  	tx.executeSql('SELECT last_insert_rowid() as value', [], function (tx, results) {
	  			id = results.rows.item(0).value;
				console.log('Добавлена запись. ID = '+id);
			});
	  });
	return id;
}

var appendTestData = function() {
	insertRecord('Аютов Марат Айсович', 'Домодедово', '9162216529');
	insertRecord('Чингачгук Моисей Соломонович', 'Москва', '0000000');
	loadFromDB();
};

var clearData = function() {
	db.transaction(function (tx) {
		tx.executeSql('DELETE FROM PHONE_BOOK', []);
	});
	clearTable();
	console.log('Таблица очищена');
};

var importFromCSV = function() {
	var input = document.createElement('input');
	input.type = 'file';

	input.onchange = function() {
		console.log(this.value);
		var reader = new FileReader();
		reader.onload = function(finfo) {
		    var allTextLines = finfo.target.result.split(/\r\n|\n/);
			clearData();
			clearTable();
		    for (var i = 0; i < allTextLines.length; i++) {
				var entries = allTextLines[i].split(',');	    	
				var id = insertRecord(entries[0], entries[1], entries[2]);
				tableBody.appendChild(appendRow("", entries[0], entries[1], entries[2]));
		    }
		    loadFromDB();
		};
		reader.readAsText(this.files[0]);
	};

	input.click();
}

var search = function() {
	var fio = document.getElementById('filterFio');
	var address = document.getElementById('filterAddress');
	var phone = document.getElementById('filterPhone');

	if (fio.value.length < 3 && address.value.length < 3 && phone.value.length < 3) {
		alert('Поисковые критерии не заданы или длина заданных значений меньше 3!');
		return;
	} 

	var sql = 'SELECT * FROM PHONE_BOOK WHERE 1 = 1 ';
	var parameters = []
	if (fio.value != "") {
		sql += ' AND FIO LIKE ?'
		parameters.push("%"+fio.value+"%");
	} 
	if (address.value != "") {
		sql += ' AND ADDRESS LIKE ?'
		parameters.push("%"+address.value+"%");
	} 
	if (phone.value != "") {
		sql += ' AND PHONE LIKE ?'
		parameters.push("%"+phone.value+"%");
	} 
	fetchData(sql, parameters);
}

loadFromDB();
